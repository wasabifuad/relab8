$(document).ready(function() {
    var item1, title1, author1, publisher1, bookLink1, bookImg1, bookIsbn1, description, viewerUrl
    var outputList = document.getElementById("list-output");
    var bookUrl = "https://www.googleapis.com/books/v1/volumes?q="
    var placeHldr = '<img src="https://via.placeholder.com/150">'
    var searchData;

    //listeener for search button
    $("#search").click(function() {
        outputList.innerHTML = "";
        document.body.style.backgroundImage = "url('')";
        searchData = $("#search-box").val();

        if (searchData === "" || searchData === null) {
            displayError();
        } else {
            //
            //
            $.ajax({
                url: bookUrl + searchData,
                dataType: "json",
                success: function(response) {
                        console.log(response)
                        if (response.totalItem === 0) {
                            alert("no result, try another search");
                        } else {
                            // $("#title").anitem({'margin-top: 10px'},1000);
                            $(".book-list").css("visibility", "visible");
                            displayResults(response);
                        }
                    }
                    // error: function(){
                    // 	alert("Something went wrong...");
                    // }
            })
        }
        $("search-box").val("");
    });

    function displayResults(response) {
        for (var i = 0; i < response.items.length; i++) {
            item1 = response.items[i];
            title1 = item1.volumeInfo.title;
            author1 = item1.volumeInfo.authors;
            bookLink1 = item1.volumeInfo.previewLink;
            publisher1 = item1.volumeInfo.publisher;
            try { bookIsbn1 = item1.volumeInfo.industryIdentifiers[1].identifier; } catch (err) { bookIsbn1 = 0; }
            bookImg1 = item1.volumeInfo.imageLinks.thumbnail;
            viewerUrl = item1.volumeInfo.previewLink;
            description = item1.volumeInfo.description;
            outputList.innerHTML += '<div class="row mt-4">' + formatOutput(bookImg1, title1, author1, description, publisher1, bookLink1, viewerUrl) + '</div>';
            // outputList.append(title1);
        }
    }

    function formatOutput(bookImg, title, author, description, publisher, bookLink, viewerUrl) {
        var htmlCard = `<div class="col-lg-6">
                        <div class="row no-gutters">
                        <div class="col-md-4">
                        <img src="${bookImg}" class="card-img" alt="...">
                        <a target="_blank" href="${viewerUrl}" class="btn btn-primary">Read More  : ${title}</a>
                        </div>
                        <div class="col-md-8">
                        <div class="card-body">
                        <h5 class="card-title">${title}</h5>
                        <p class="card-text">Author : ${author}</p>
                        <p class="card-text">Publisher : ${publisher}</p>
                        <p class="card-text">Description : ${description}</p>
                        </div>
                        </div>
                        </div>
                        </div>
                        </div>`
        return htmlCard;
    }
    //handling error;
    function displayError() {
        alert("search item cannot be empty");
    }
})