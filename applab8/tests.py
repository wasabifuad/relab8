from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from . import views
from .views import index, redirecting
from django.http import HttpRequest
class Test_Story8(TestCase) :
    def test_link_exist(self):
        response = Client().get('/index/')
        self.assertEqual(response.status_code, 200)
    def test_home_url_is_notexist(self):
        response = Client().get('/home/')
        self.assertEqual(response.status_code, 404)

    def test_using_func(self):
        response = resolve('/index/')
        self.assertEqual(response.func, views.index)

    def test_using_template(self):
        response = Client().get('/index/')
        self.assertTemplateUsed(response, 'index.html')

    def test_landing_page_title_is_right(self):
        request = HttpRequest()
        response = index(request)
        html_response = response.content.decode('utf8')
        self.assertIn('<title>kinda Book Finder</title>', html_response)

    def test_landing_page_redirecting(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
