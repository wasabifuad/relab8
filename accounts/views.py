from django.shortcuts import render, redirect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout

# Create your views here.
def account(req):
    error = "Username atau Password anda salah"
    user = req.user
    if user.is_authenticated:
        return redirect('accounts:login')
    if req.method == 'POST':
        form = AuthenticationForm(data=req.POST)
        if form.is_valid():
            user = form.get_user()
            login(req, user)
            return redirect('accounts:login')
    else:
        form = AuthenticationForm()
    return render(req, 'account.html', {'form':form, 'error':error})

def log_in(req):
    return render(req, 'login.html')

def log_out(req):
    logout(req)
    return redirect('accounts:acc')
