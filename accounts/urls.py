from django.urls import path
from . import views

app_name = "accounts"

urlpatterns = [
    path('acc', views.account, name="acc"),
    path('login', views.log_in, name="login"),
    path('logout', views.log_out, name='logout')
]
