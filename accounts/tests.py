from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from accounts.views import *
from django.contrib.auth.models import User
# Create your tests here.

class UnitTestStory9(TestCase):

	@classmethod
	def setUpTestData(cls):
		cls.u1 = User.objects.create_user(username='testuser', password='testpassword')

	def test_notexist_url_is_notexist(self):
		response = Client().get('/gaada/')
		self.assertEqual(response.status_code, 404)

	# def test_loginpage_url_is_exist_use_login_template(self):
	# 	response = Client().get('/acc/')
	# 	self.assertEqual(response.status_code, 200)
	# 	self.assertTemplateUsed(response, 'login.html')

	# def test_loginpage_using_login_function(self):
	# 	response = resolve('/acc/')
	# 	self.assertEqual(response.func, loginpage)

	def test_loginpage_title_is_right(self):
		request = HttpRequest()
		response = log_in(request)
		html_response = response.content.decode('utf8')
		self.assertIn('<title>kinda Book Finder</title>', html_response)
    #
	# def test_landingpage_url_is_exist_use_landing_template(self):
	# 	response = Client().get('/account/')
	# 	self.assertEqual(response.status_code, 200)
	# 	self.assertTemplateUsed(response, 'account.html')
    # def test_link_exist(self):
    #     response = Client().get('/account/')
    #     self.assertEqual(response.status_code, 200)
    # def test_home_url_is_notexist(self):
    #     response = Client().get('/home/')
    #     self.assertEqual(response.status_code, 404)
    # def test_using_func(self):
    #     response = resolve('/account/')
    #     self.assertEqual(response.func, views.account)
    # def test_using_template(self):
    #     response = Client().get('/account/')
    #     self.assertTemplateUsed(response, 'account.html')
    # def test_landing_page_title_is_right(self):
    #     request = HttpRequest()
    #     response = account(request)
    #     html_response = response.content.decode('utf8')
    #     self.assertIn('<title>kinda Book Finder</title>', html_response)
    # def test_landing_page_redirecting(self):
    #     response = Client().get('/')
    #     self.assertEqual(response.status_code, 302)
